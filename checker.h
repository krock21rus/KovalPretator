#pragma once

int check_pos = 0;
vector<polnode> poliz;
map<string, int> vartoint;
int varcnt = 0;

map<string, vector<keytype>> vars;
vector<vector<string>> names;

void testlex(const vector<lexem> &vec, string name) {
  //cerr << check_pos << endl;
  if (check_pos >= vec.size())
    throw myex("unexpected end");
  if (vec[check_pos].name != name)
    throw myex("unexpected " + vec[check_pos].name + ", expected " + name);
  check_pos++;
}

int toi(string s) {
  int ans = 0;
  for (auto c : s) {
    ans *= 10;
    ans += c - '0';
  }
  return ans;
}

double todouble(string s) {
  return 2.0;
}

bool tobool(string s) {
  if (s == "true") return true;
  return false;
}

void addoper(string op) {
  polnode add;
  add.ptype = POLOPER;
  add.oper = op;
  poliz.push_back(add);
}

keytype check_expression(const vector<lexem> &vec, int lvl = 8) {
  if (check_pos >= vec.size())
    throw myex("unexpected end in check_expression");
  if (vec[check_pos].name == "(") {
    check_pos++;
    keytype t1 = check_expression(vec);
    testlex(vec, ")");
    return t1;
  } else if (lvl == -1) {
    keytype t1;
    if (vec[check_pos].type != CONST && vec[check_pos].type != NAME && vec[check_pos].name != "cinout" &&
        vec[check_pos].name != "cin" && vec[check_pos].name != "cout")
      return keytype(EMPTY, 0);
    if (vec[check_pos].name == "true" || vec[check_pos].name == "false") t1 = keytype(BOOL, 0);
    else if (vec[check_pos].type == CONST) t1 = keytype(INT, 0);
    else if (vec[check_pos].name == "cin" || vec[check_pos].name == "cout" || vec[check_pos].name == "cinout")
      t1 = keytype(CINOUT, 0);
    else if (vec[check_pos].type == NAME) {
      if (vars[vec[check_pos].name].empty()) {
        throw myex("does not have a name " + vec[check_pos].name);
      } else {
        t1 = vars[vec[check_pos].name].back();
      }
    } else {
      t1 = keytype(CINOUT, 0);
    }
    polnode p;
    p.ptype = POLADD;
    p.ktype = t1;
    if (t1.isvar) {
      p.kval.ival = vartoint[vec[check_pos].name];
    } else {
      if (t1.type == INT) p.kval.ival = toi(vec[check_pos].name);
      if (t1.type == DOUBLE) p.kval.dval = todouble(vec[check_pos].name);
      if (t1.type == BOOL) p.kval.bval = tobool(vec[check_pos].name);
    }
    poliz.push_back(p);
    check_pos++;
    return t1;
  } else if (lvl == 0) { // unary
    if (vec[check_pos].name == "++" || vec[check_pos].name == "--") {
      string s = vec[check_pos].name;
      check_pos++;
      keytype checkexp = check_expression(vec, lvl);
      addoper(s);
      if (checkexp.type != INT || !checkexp.isvar)
        throw myex("bad type for ++\\--");
      return keytype(INT, 1);
    } else if (vec[check_pos].name == "!") {
      string s = vec[check_pos].name;
      check_pos++;
      keytype nexttype = check_expression(vec, lvl);
      addoper(s);
      if (nexttype.type != BOOL)
        throw myex("bad type for !");
      return keytype(BOOL, 0);
    } else if (vec[check_pos].name == "+" || vec[check_pos].name == "-") { // "+u", "-u"
      string s = vec[check_pos].name + "u";
      check_pos++;
      keytype nexttype = check_expression(vec, lvl);
      addoper(s);
      if (nexttype.type != INT && nexttype.type != DOUBLE)
        throw myex("bad type for +\\-");
      nexttype.isvar = false;
      return nexttype;
    } else return check_expression(vec, lvl - 1);
  } else if (lvl == 1) {
    keytype t1 = check_expression(vec, lvl - 1);
    while (true) {
      if (vec[check_pos].name == "%") {
        string s = vec[check_pos].name;
        check_pos++;
        keytype t2 = check_expression(vec, lvl - 1);
        addoper(s);
        if (t1.type != INT || t2.type != INT)
          throw myex("bad type for %");
        t1 = keytype(INT, 0);
      } else if (vec[check_pos].name == "*" || vec[check_pos].name == "/") { // "*", "/"
        string s = vec[check_pos].name;
        check_pos++;
        keytype t2 = check_expression(vec, lvl - 1);
        addoper(s);
        if (t1.type != INT && t1.type != DOUBLE || t2.type != INT && t2.type != DOUBLE)
          throw myex("bad type for *\\/");
        if (t1.type == DOUBLE || t2.type == DOUBLE) t1.type = DOUBLE;
        t1.isvar = false;
      } else break;
    }
    return t1;
  } else if (lvl == 2) {
    keytype t1 = check_expression(vec, lvl - 1);
    while (true) {
      if (vec[check_pos].name == "+" || vec[check_pos].name == "-") {
        string s = vec[check_pos].name;
        check_pos++;
        keytype t2 = check_expression(vec, lvl - 1);
        addoper(s);
        if (t1.type != INT && t1.type != DOUBLE || t2.type != INT && t2.type != DOUBLE)
          throw myex("bad type for +\\-");
        if (t1.type == DOUBLE || t2.type == DOUBLE) t1.type = DOUBLE;
        t1.isvar = false;
      } else break;
    }
    return t1;
  } else if (lvl == 3) {
    keytype t1 = check_expression(vec, lvl - 1);
    while (true) {
      if (vec[check_pos].name == "<<" || vec[check_pos].name == ">>") {
        string s = vec[check_pos].name;
        check_pos++;
        if (t1.type != CINOUT)
          throw myex("bad cinout for <<\\>>");
        keytype t2 = check_expression(vec, lvl - 1);
        addoper(s);
        if (t2.type != INT && t2.type != DOUBLE && t2.type != BOOL)
          throw myex("bad type for <<\\>>");
        t1 = keytype(CINOUT, 0);
      } else break;
    }
    return t1;
  } else if (lvl == 4) {
    keytype t1 = check_expression(vec, lvl - 1);
    while (true) {
      if (vec[check_pos].name == "<" || vec[check_pos].name == "<=" || vec[check_pos].name == ">" ||
          vec[check_pos].name == ">=") {
        string s = vec[check_pos].name;
        check_pos++;
        keytype t2 = check_expression(vec, lvl - 1);
        addoper(s);
        if (t1.type == BOOL && t2.type == BOOL || t1.type != BOOL && t2.type != BOOL)
          t1 = keytype(BOOL, 0);
        else throw myex("bad type for <\\>\\<=\\>=");\

      } else break;
    }
    return t1;
  } else if (lvl == 5) {
    keytype t1 = check_expression(vec, lvl - 1);
    while (true) {
      if (vec[check_pos].name == "==" || vec[check_pos].name == "!=") {
        string s = vec[check_pos].name;
        check_pos++;
        keytype t2 = check_expression(vec, lvl - 1);
        addoper(s);
        if (t1.type == BOOL && t2.type == BOOL || t1.type != BOOL && t2.type != BOOL)
          t1 = keytype(BOOL, 0);
        else throw myex("bad type for ==\\!=");
      } else break;
    }
    return t1;
  } else if (lvl == 6) {
    keytype t1 = check_expression(vec, lvl - 1);
    while (true) {
      if (vec[check_pos].name == "&&") {
        string s = vec[check_pos].name;
        check_pos++;
        keytype t2 = check_expression(vec, lvl - 1);
        addoper(s);
        if (t1.type == BOOL && t2.type == BOOL)
          t1 = keytype(BOOL, 0);
        else throw myex("bad type for &&");
      } else break;
    }
    return t1;
  } else if (lvl == 7) {
    keytype t1 = check_expression(vec, lvl - 1);
    while (true) {
      if (vec[check_pos].name == "||") {
        string s = vec[check_pos].name;
        check_pos++;
        keytype t2 = check_expression(vec, lvl - 1);
        addoper(s);
        if (t1.type == BOOL && t2.type == BOOL)
          t1 = keytype(BOOL, 0);
        else throw myex("bad type for ||");
      } else break;
    }
    return t1;
  } else if (lvl == 8) {
    keytype t1 = check_expression(vec, lvl - 1);
    if (vec[check_pos].name == "=") {
      string s = vec[check_pos].name;
      check_pos++;
      keytype t2 = check_expression(vec, lvl);
      addoper(s);
      if (t1.isvar &&
          ((t1.type == INT && t2.type == INT) || (t1.type == DOUBLE && (t2.type == INT || t2.type == DOUBLE)) ||
           (t1.type == BOOL && t2.type == BOOL))) {
        t1.isvar = false;
        return t1;
      } else throw myex("bad type for =");
    } else return t1;
  }
  return keytype(EMPTY, 0);
}

void check_oper(const vector<lexem> &vec, bool isend = true) {
  if (check_pos >= vec.size())
    throw myex("unexpected end in check_oper");
  if (vec[check_pos].name == "{") {
    check_pos++;
    names.push_back(vector<string>());
    do {
      check_oper(vec);
      polnode p;
      p.ptype = POLCLEAR;
      poliz.push_back(p);
    } while (check_pos < vec.size() && vec[check_pos].name != "}");
    testlex(vec, "}");
    for (string s : names.back()) {
      polnode p;
      p.ptype = POLDELVAR;
      p.kval.ival = vartoint[s];
      poliz.push_back(p);
      vars[s].pop_back();
    }
    names.pop_back();
  } else if (vec[check_pos].name == "for") {
    check_pos++;
    names.push_back(vector<string>());
    testlex(vec, "(");
    check_oper(vec, false);
    testlex(vec, ";");
    int reppos = poliz.size();
    keytype iftype = check_expression(vec);
    if (iftype.type != BOOL && iftype.type != EMPTY)
      throw myex("bad if in check_oper for");
    testlex(vec, ";");
    polnode ptoend;
    ptoend.ptype = POLGOTOFALSE;
    ptoend.kval.ival = -1;
    int ptoendpos = poliz.size();
    poliz.push_back(ptoend);
    int startpos = int(poliz.size());
    check_expression(vec);
    vector<polnode> step;
    for (int i = startpos; i < poliz.size(); ++i) {
      step.push_back(poliz[i]);
    }
    poliz.resize(startpos);
    testlex(vec, ")");
    check_oper(vec);
    for (int i = 0; i < step.size(); ++i) {
      poliz.push_back(step[i]);
    }
    polnode p;
    p.ptype = POLGOTO;
    p.kval.ival = reppos;
    poliz.push_back(p);
    ptoend.kval.ival = int(poliz.size());
    poliz[ptoendpos] = ptoend;
    /*if (check_pos < vec.size() && vec[check_pos].name == "else") {
      check_pos++;
      check_oper(vec);
    }*/

    for (string s : names.back()) {
      polnode p;
      p.ptype = POLDELVAR;
      p.kval.ival = vartoint[s];
      poliz.push_back(p);
      vars[s].pop_back();
    }
    names.pop_back();
  } else if (vec[check_pos].name == "while") {
    check_pos++;
    testlex(vec, "(");
    int ppos = int(poliz.size());
    keytype iftype = check_expression(vec);
    if (iftype.type != BOOL)
      throw myex("bad if in check_oper while");
    testlex(vec, ")");
    polnode ptoend;
    ptoend.ptype = POLGOTOFALSE;
    ptoend.kval.ival = -1;
    int ptoendpos = int(poliz.size());
    poliz.push_back(ptoend);
    check_oper(vec);
    polnode ptostart;
    ptostart.ptype = POLGOTO;
    ptostart.kval.ival = ppos;
    poliz.push_back(ptostart);
    ptoend.kval.ival = int(poliz.size());
    poliz[ptoendpos] = ptoend;
    /*if (check_pos < vec.size() && vec[check_pos].name == "else") {
      check_pos++;
      check_oper(vec);
    }*/
  } else if (vec[check_pos].name == "do") {//TODO
    check_pos++;
    check_oper(vec);
    testlex(vec, "while");
    testlex(vec, "(");
    if (check_expression(vec).type != BOOL)
      throw myex("bad if in check_oper dowhile");
    testlex(vec, ")");
  } else if (vec[check_pos].name == "if") {
    check_pos++;
    testlex(vec, "(");
    if (check_expression(vec).type != BOOL)
      throw myex("bad if in check_oper if");
    testlex(vec, ")");
    polnode ptoend;
    ptoend.ptype = POLGOTOFALSE;
    ptoend.kval.ival = -1;
    int ptoendpos = int(poliz.size());
    poliz.push_back(ptoend);
    check_oper(vec);
    ptoend.kval.ival = int(poliz.size());
    poliz[ptoendpos] = ptoend;
    if (check_pos < vec.size() && vec[check_pos].name == "else") {
      poliz[ptoendpos].kval.ival++;
      ptoend.ptype = POLGOTO;
      ptoend.kval.ival = -1;
      ptoendpos = int(poliz.size());
      poliz.push_back(ptoend);
      check_pos++;
      check_oper(vec);
      ptoend.kval.ival = int(poliz.size());
      poliz[ptoendpos] = ptoend;
    }
  } else if (vec[check_pos].name == "int" || vec[check_pos].name == "bool" || vec[check_pos].name == "double") {
    keytype curtype = keytype(INT, 1);
    if (vec[check_pos].name == "bool") curtype = keytype(BOOL, 1);
    if (vec[check_pos].name == "double") curtype = keytype(DOUBLE, 1);
    do {
      check_pos++;
      if (check_pos >= vec.size())
        throw myex("unexpected end in description");
      if (vec[check_pos].type != NAME)
        throw myex("unexpected name in description");
      vars[vec[check_pos].name].push_back(curtype);
      polnode p;
      p.ptype = POLCREATEVAR;
      p.kval.ival = varcnt;
      p.ktype = curtype;
      poliz.push_back(p);
      vartoint[vec[check_pos].name] = varcnt++;
      names[names.size() - 1].push_back(vec[check_pos].name);
      check_pos++;
      if (check_pos >= vec.size())
        throw myex("unexpected end in description");
      if (vec[check_pos].name == "=") {
        check_pos--;
        check_expression(vec);
      }
      if (check_pos >= vec.size())
        throw myex("unexpected end in description");
    } while (check_pos < vec.size() && vec[check_pos].name == ",");
    if (isend)
      testlex(vec, ";");
  } else if (vec[check_pos].name == "else") {
    throw myex("Who has lost \"else\"?");
  } else {
    check_expression(vec);
    if (isend) {
      testlex(vec, ";");
      polnode p;
      p.ptype = POLCLEAR;
      poliz.push_back(p);
    }
  }
}

struct stackitem {
    bool isvar;
    types type;
    var val;
};

map<int, vector<var>> varmap;

vector<stackitem> st;

void run_poliz() {
  var nullvar;
  int curpos = 0;
  while (curpos < poliz.size()) {
    //if (curpos == 35)
    //  cerr << curpos << endl;
    if (poliz[curpos].ptype == POLCREATEVAR) {
      varmap[poliz[curpos].kval.ival].push_back(nullvar);
      curpos++;
    } else if (poliz[curpos].ptype == POLDELVAR) {
      varmap[poliz[curpos].kval.ival].pop_back();
      curpos++;
    } else if (poliz[curpos].ptype == POLCLEAR) {
      st.clear();
      curpos++;
    } else if (poliz[curpos].ptype == POLADD) {
      stackitem sti;
      sti.type = poliz[curpos].ktype.type;
      if (poliz[curpos].ktype.isvar) {
        sti.isvar = true;
        sti.val.ival = poliz[curpos].kval.ival;
      } else {
        sti.isvar = false;
        sti.val = poliz[curpos].kval;
      }
      st.push_back(sti);
      curpos++;
    } else if (poliz[curpos].ptype == POLGOTO) {
      curpos = poliz[curpos].kval.ival;
    } else if (poliz[curpos].ptype == POLGOTOFALSE) {
      if (st.empty()) exit(2);
      bool isgo = !(*st.rbegin()).val.bval;
      st.pop_back();
      if (isgo) curpos = poliz[curpos].kval.ival;
      else curpos++;
    } else if (poliz[curpos].ptype == POLOPER) {
      if (poliz[curpos].oper == "+u") { // 1
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        if (s2.type == INT) s2.val.ival = +s2.val.ival;
        else if (s2.type == DOUBLE) s2.val.dval = +s2.val.dval;
        else if (s2.type == BOOL) s2.val.bval = +s2.val.bval;
        st.push_back(s2);
      } else if (poliz[curpos].oper == "-u") { // 2
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        if (s2.type == INT) s2.val.ival = -s2.val.ival;
        else if (s2.type == DOUBLE) s2.val.dval = -s2.val.dval;
        else if (s2.type == BOOL) s2.val.bval = -s2.val.bval;
        st.push_back(s2);
      } else if (poliz[curpos].oper == "++") { // 3
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        if (s2.isvar) {
          if (s2.type == INT) varmap[s2.val.ival].back().ival++;
          else if (s2.type == DOUBLE) varmap[s2.val.ival].back().dval++;
          else if (s2.type == BOOL) varmap[s2.val.ival].back().bval++;
        } else exit(1);
        st.push_back(s2);
      } else if (poliz[curpos].oper == "--") { // 4
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        if (s2.isvar) {
          if (s2.type == INT) varmap[s2.val.ival].back().ival--;
          else if (s2.type == DOUBLE) varmap[s2.val.ival].back().dval--;
          //else if (s2.type == BOOL) varmap[s2.val.ival].back().bval--;
        } else exit(1);
        st.push_back(s2);
      } else if (poliz[curpos].oper == "+" || poliz[curpos].oper == "-" || poliz[curpos].oper == "*" ||
                 poliz[curpos].oper == "/" || poliz[curpos].oper == "%") { // 5
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        stackitem s1 = (*st.rbegin());
        st.pop_back();
        var v1, v2;
        if (s2.isvar) {
          v2 = varmap[s2.val.ival].back();
        } else {
          v2 = s2.val;
        }
        if (s1.isvar) {
          v1 = varmap[s1.val.ival].back();
        } else {
          v1 = s1.val;
        }
        if (poliz[curpos].oper == "+") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.ival = v1.ival + v2.ival, s1.type = INT;
            else if (s2.type == DOUBLE) s1.val.dval = v1.ival + v2.dval, s1.type = DOUBLE;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival + v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.dval = v1.dval + v2.ival, s1.type = DOUBLE;
            else if (s2.type == DOUBLE) s1.val.dval = v1.dval + v2.dval, s1.type = DOUBLE;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval + v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval + v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval + v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval + v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == "-") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.ival = v1.ival - v2.ival, s1.type = INT;
            else if (s2.type == DOUBLE) s1.val.dval = v1.ival - v2.dval, s1.type = DOUBLE;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival - v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.dval = v1.dval - v2.ival, s1.type = DOUBLE;
            else if (s2.type == DOUBLE) s1.val.dval = v1.dval - v2.dval, s1.type = DOUBLE;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval - v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval - v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval - v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval - v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == "*") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.ival = v1.ival * v2.ival, s1.type = INT;
            else if (s2.type == DOUBLE) s1.val.dval = v1.ival * v2.dval, s1.type = DOUBLE;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival * v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.dval = v1.dval * v2.ival, s1.type = DOUBLE;
            else if (s2.type == DOUBLE) s1.val.dval = v1.dval * v2.dval, s1.type = DOUBLE;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval * v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval * v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval * v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval * v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == "/") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.ival = v1.ival / v2.ival, s1.type = INT;
            else if (s2.type == DOUBLE) s1.val.dval = v1.ival / v2.dval, s1.type = DOUBLE;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival / v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.dval = v1.dval / v2.ival, s1.type = DOUBLE;
            else if (s2.type == DOUBLE) s1.val.dval = v1.dval / v2.dval, s1.type = DOUBLE;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval / v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval / v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval / v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval / v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == "%") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.ival = v1.ival % v2.ival, s1.type = INT;
            //else if (s2.type == DOUBLE) s1.val.dval = v1.ival % v2.dval, s1.type = DOUBLE;
            //else if (s2.type == BOOL) s1.val.bval = bool(v1.ival % v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            //if (s2.type == INT) s1.val.dval = v1.dval % v2.ival, s1.type = DOUBLE;
            //else if (s2.type == DOUBLE) s1.val.dval = v1.dval % v2.dval, s1.type = DOUBLE;
            //else if (s2.type == BOOL) s1.val.bval = bool(v1.dval % v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            //if (s2.type == INT) s1.val.bval = bool(v1.bval % v2.ival), s1.type = BOOL;
            //else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval % v2.dval), s1.type = BOOL;
            //else if (s2.type == BOOL) s1.val.bval = v1.bval % v2.bval, s1.type = BOOL;
          }
        }
        s1.isvar = false;
        st.push_back(s1);
      } else if (poliz[curpos].oper == "<<" || poliz[curpos].oper == ">>") { // 13
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        stackitem s1 = (*st.rbegin());
        st.pop_back();
        if (s2.isvar) {
          if (poliz[curpos].oper == "<<") {
            if (s2.type == INT) cout << varmap[s2.val.ival].back().ival;
            else if (s2.type == DOUBLE) cout << varmap[s2.val.ival].back().dval;
            else if (s2.type == BOOL) cout << varmap[s2.val.ival].back().bval;
            cout << endl;
          }
          if (poliz[curpos].oper == ">>") {
            if (s2.type == INT) cin >> varmap[s2.val.ival].back().ival;
            else if (s2.type == DOUBLE) cin >> varmap[s2.val.ival].back().dval;
            else if (s2.type == BOOL) cin >> varmap[s2.val.ival].back().bval;
          }
        } else if (poliz[curpos].oper == "<<") {
          if (s2.type == INT) cout << s2.val.ival;
          else if (s2.type == DOUBLE) cout << s2.val.dval;
          else if (s2.type == BOOL) cout << s2.val.bval;
          cout << endl;
        } else
          exit(1);
        st.push_back(s1);
      } else if (poliz[curpos].oper == "<" || poliz[curpos].oper == "<=" || poliz[curpos].oper == ">" ||
                 poliz[curpos].oper == ">=" || poliz[curpos].oper == "==" || poliz[curpos].oper == "!=") { // 15
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        stackitem s1 = (*st.rbegin());
        st.pop_back();
        var v1, v2;
        if (s2.isvar) {
          v2 = varmap[s2.val.ival].back();
        } else {
          v2 = s2.val;
        }
        if (s1.isvar) {
          v1 = varmap[s1.val.ival].back();
        } else {
          v1 = s1.val;
        }
        if (poliz[curpos].oper == "<") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.bval = v1.ival < v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.ival < v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival < v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.bval = v1.dval < v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.dval < v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval < v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval < v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval < v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval < v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == "<=") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.bval = v1.ival <= v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.ival <= v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival <= v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.bval = v1.dval <= v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.dval <= v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval <= v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval <= v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval <= v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval <= v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == ">") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.bval = v1.ival > v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.ival > v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival > v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.bval = v1.dval > v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.dval > v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval > v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval > v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval > v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval > v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == ">=") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.bval = v1.ival >= v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.ival >= v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival >= v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.bval = v1.dval >= v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.dval >= v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval >= v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval >= v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval >= v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval >= v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == "==") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.bval = v1.ival == v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.ival == v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival == v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.bval = v1.dval == v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.dval == v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval == v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval == v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval == v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval == v2.bval, s1.type = BOOL;
          }
        }
        if (poliz[curpos].oper == "!=") {
          if (s1.type == INT) {
            if (s2.type == INT) s1.val.bval = v1.ival != v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.ival != v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.ival != v2.bval), s1.type = BOOL;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) s1.val.bval = v1.dval != v2.ival, s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = v1.dval != v2.dval, s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = bool(v1.dval != v2.bval), s1.type = BOOL;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) s1.val.bval = bool(v1.bval != v2.ival), s1.type = BOOL;
            else if (s2.type == DOUBLE) s1.val.bval = bool(v1.bval != v2.dval), s1.type = BOOL;
            else if (s2.type == BOOL) s1.val.bval = v1.bval != v2.bval, s1.type = BOOL;
          }
        }
        s1.isvar = false;
        st.push_back(s1);
      } else if (poliz[curpos].oper == "&&") { // 21
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        stackitem s1 = (*st.rbegin());
        st.pop_back();
        if (s1.type != BOOL || s2.type != BOOL)
          exit(1);
        s1.val.bval = s1.val.bval && s2.val.bval;
        s1.isvar = false;
        st.push_back(s1);
      } else if (poliz[curpos].oper == "||") { // 22
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        stackitem s1 = (*st.rbegin());
        st.pop_back();
        if (s1.type != BOOL || s2.type != BOOL)
          exit(1);
        s1.val.bval = s1.val.bval || s2.val.bval;
        s1.isvar = false;
        st.push_back(s1);
      } else if (poliz[curpos].oper == "=") { // 23
        stackitem s2 = (*st.rbegin());
        st.pop_back();
        stackitem s1 = (*st.rbegin());
        st.pop_back();
        var v2;
        if (s2.isvar) {
          v2 = varmap[s2.val.ival].back();
        } else {
          v2 = s2.val;
        }
        if (s1.isvar) {
          if (s1.type == INT) {
            if (s2.type == INT) varmap[s1.val.ival].back().ival = v2.ival;
            else if (s2.type == DOUBLE) varmap[s1.val.ival].back().ival = v2.dval;
            else if (s2.type == BOOL) varmap[s1.val.ival].back().ival = v2.bval;
          } else if (s1.type == DOUBLE) {
            if (s2.type == INT) varmap[s1.val.ival].back().dval = v2.ival;
            else if (s2.type == DOUBLE) varmap[s1.val.ival].back().dval = v2.dval;
            else if (s2.type == BOOL) varmap[s1.val.ival].back().dval = v2.bval;
          } else if (s1.type == BOOL) {
            if (s2.type == INT) varmap[s1.val.ival].back().bval = v2.ival;
            else if (s2.type == DOUBLE) varmap[s1.val.ival].back().bval = v2.dval;
            else if (s2.type == BOOL) varmap[s1.val.ival].back().bval = v2.bval;
          }
          s1.isvar = false;
          st.push_back(s1);
        } else
          exit(1);
      } else
        exit(2);
      curpos++;
    }
  }
}