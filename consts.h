void err(int mypos, string s) {
  cerr << "Error! " << s << endl;
  const int l = 50;
  int cmypos = mypos;
  while (mypos > 0 && program[mypos] != '\n') mypos--;
  for (int i = mypos + 1; i < program.size(); ++i) {
    if (program[i] == '\n' || i - mypos > l) break;
    if (cmypos == i) cerr << 'V';
    else cerr << ' ';
  }
  cerr << endl;
  for (int i = mypos + 1; i < program.size(); ++i) {
    if (program[i] == '\n' || i - mypos > l) break;
    cerr << program[i];
  }
  exit(0);
}

enum chartype {
    cBAD, cSPACE, cALNUM, cPUNCT, cOPER
};

enum lextype {
    NAME, CONST, OPER, PUNCT, KEYWORD, XZ
};


enum types {
    INT, DOUBLE, BOOL, CINOUT, EMPTY
};

struct keytype {
    types type;
    bool isvar;

    keytype() {
      type = EMPTY;
      isvar = false;
    }

    keytype(types t, bool b) {
      type = t;
      isvar = b;
    }
};

enum poltype {
    POLOPER, POLADD, POLCREATEVAR, POLCLEAR, POLGOTO, POLGOTOFALSE, POLDELVAR
};

union var {
    int ival;
    double dval;
    bool bval;
};

struct polnode {
    poltype ptype;
    string oper;
    keytype ktype;
    var kval;
};

chartype typec[126];
unordered_set<string> keywords;
unordered_set<string> opers;
unordered_set<string> puncts;
unordered_set<string> priors[20] = {
        {"++", "--", "+u", "-u", "!"}, // 0 -++i
        {"*",  "/",  "%"}, // 1
        {"+",  "-"}, // 2
        {"<<", ">>"}, // 3
        {"<",  "<=", ">",  ">="}, // 4
        {"==", "!="}, // 5
        {"&&"}, // 6
        {"||"}, // 7
        {"="}, // 8
        {}, // 9
        {}, // 10
};

void init_consts() {

  for (char i = 0; i < 126; ++i) {
    if (isspace(i)) {
      typec[i] = cSPACE;
    } else if (isalnum(i) || i == '_') {
      typec[i] = cALNUM;
    } else if (i == '{' || i == '}' || i == '(' || i == ')' || i == ';' || i == ',' || i == '[' || i == ']' ||
               i == '#' || i == '.') {
      typec[i] = cPUNCT;
    } else if (i == '+' || i == '-' || i == '*' || i == '/' || i == '%' || i == '!' || i == '=' || i == '<' ||
               i == '>' || i == '|' || i == '&') {
      typec[i] = cOPER;
    } else {
      typec[i] = cBAD;
    }
  }
  vector<string> keywords = {"for", "if", "while", "do", "int", "bool", "double", "else"};
  for (auto s : keywords) {
    ::keywords.insert(s);
  }
  vector<string> opers = {"+", "-", "||", "*", "/", "&&", "%", "=", "<", ">", "==", "!=", "<=", ">=", "++", "--",
                          "<<", ">>", "!", "="};

  for (auto s : opers) {
    ::opers.insert(s);
  }
  vector<string> puncts = {"{", "}", "[", "]", "(", ")", ",", ";"};
  for (auto s : puncts) {
    ::puncts.insert(s);
  }
}