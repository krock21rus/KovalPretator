#pragma once

#include <bits/stdc++.h>

using namespace std;

class myex : public exception {
public:
    string s;

    myex(string s) : s(s) {}

    virtual const char *what() const throw() {
      return s.c_str();
    }
};