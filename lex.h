#pragma once

#include <bits/stdc++.h>
#include "ex.h"

using namespace std;

struct lexem {
    lextype type;
    string name;
};

vector<lexem> get_lex() {
  vector<lexem> ans;
  for (int i = 0; i < program.size();) {
    char c = program[i];
    if (typec[c] == cBAD) {
      err(i, "Bad symbol");
    }
    if (typec[c] == cSPACE) {
      i++;
      continue;
    }
    string s = "";
    int j = i;
    if (typec[c] == cPUNCT) {
      s.push_back(c);
      j++;
    } else if (typec[c] == cOPER) {
      s.push_back(c);
      j++;
      if (j < program.size() && ((program[j] == c || c == '!' && program[j] == '=') ||
                                 (program[j] == c || c == '<' && program[j] == '=') ||
                                 (program[j] == c || c == '>' && program[j] == '=')))
        s.push_back(program[j]), j++;
    } else
      while (j < program.size() && typec[program[j]] == typec[c]) {
        s.push_back(program[j++]);
      }
    lexem l;
    l.name = s;
    if (keywords.count(s))
      l.type = KEYWORD;
    else if (opers.count(s))
      l.type = OPER;
    else if (puncts.count(s))
      l.type = PUNCT;
    else {
      bool isnum = true;
      for (auto cc : s) {
        if (!isdigit(cc)) isnum = false;
      }
      if (isnum || s == "true" || s == "false") {
        l.type = CONST;
      } else {
        bool isname = true;
        if (isdigit(s[0])) isname = false;
        for (auto cc : s) {
          if (typec[cc] != cALNUM) isname = false;
        }
        if (isname) {
          l.type = NAME;
        } else {
          l.type = XZ;
        }
      }
    }
    ans.push_back(l);
    i = j;
  }
  return ans;
}