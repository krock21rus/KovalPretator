﻿#define _UNICODE

#include <bits/stdc++.h>

using namespace std;
string program;

#include "consts.h"
#include "lex.h"
#include "checker.h"

string tos(int v) {
  if (v == 0) return "0";
  string ans = "";
  while (v != 0) {
    ans.push_back(v % 10 + '0');
    v /= 10;
  }
  reverse(ans.begin(), ans.end());
  return ans;
}

void print_lexemes(vector<lexem> vec) {
  cout << "LEXEMES:\n";
  for (auto i : vec) {
    cout << setw(9);
    if (i.type == NAME) cout << "NAME";
    if (i.type == CONST) cout << "CONST";
    if (i.type == OPER) cout << "OPER";
    if (i.type == PUNCT) cout << "PUNCT";
    if (i.type == KEYWORD) cout << "KEYWORD";
    if (i.type == XZ) cout << "XZ";
    cout << ": ";
    cout << "\"" << i.name << "\"\n";
  }
}

string getpol(polnode p) {
  if (p.ptype == POLOPER) return "OPER " + p.oper;
  if (p.ptype == POLGOTOFALSE) return "GOTOFALSE " + tos(p.kval.ival);
  if (p.ptype == POLGOTO) return "GOTO " + tos(p.kval.ival);
  if (p.ptype == POLCREATEVAR) return "CREATEVAR " + tos(p.kval.ival);
  if (p.ptype == POLADD) {
    string ans = "ADD ";
    if (p.ktype.isvar) ans += "VAR " + tos(p.kval.ival);
    else if (p.ktype.type == INT) ans += "INTC " + tos(p.kval.ival);
    else if (p.ktype.type == DOUBLE) ans += "DOUBLEC " + tos(p.kval.dval);
    else if (p.ktype.type == BOOL) ans += "BOOLC " + tos(p.kval.bval);
    else if (p.ktype.type == CINOUT) ans += "CINOUT";
    return ans;
  }
  if (p.ptype == POLCLEAR) return "POLCLEAR ";
  if (p.ptype == POLDELVAR) return "DELVAR " + tos(p.kval.ival);
}

void print_poliz() {
  for (int i = 0; i < poliz.size(); ++i) {
    cout << i << ": " << getpol(poliz[i]) << endl;
  }
}

int main() {
  setlocale(LC_ALL, "Rus");
  init_consts();
  try {
    ifstream fin("p.txt");
    int c = fin.get();
    while (c != -1) {
      program.push_back(c);
      c = fin.get();
    }
    vector<lexem> vec = get_lex();

    check_oper(vec);
    //print_lexemes(vec);
    //print_poliz();
    run_poliz();
  } catch (myex e) {
    cerr << e.what() << endl;
  }

  return 0;
}